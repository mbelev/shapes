namespace Shapes.Shapes
{
    public abstract class Shape
    {
        public abstract double GetPerimeter();

        public abstract void Draw();
    }
}
