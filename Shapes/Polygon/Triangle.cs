namespace Shapes.Shapes.Polygon
{
    using System;

    public class Triangle : Polygon
    {
        public Point A { get; set; }

        public Point B { get; set; }

        public Point C { get; set; }

        public Triangle(double ax, double ay, double bx, double by, double cx, double cy)
        {
            this.A = new Point(ax, ay);
            this.B = new Point(bx, by);
            this.C = new Point(cx, cy);

            bool isInvalidTriangle = ((this.A.GetDistance(this.B) + this.B.GetDistance(this.C)) <= this.C.GetDistance(this.A)) ||
                                    ((this.B.GetDistance(this.C) + this.C.GetDistance(this.A)) <= this.A.GetDistance(this.B)) ||
                                    ((this.C.GetDistance(this.A) + this.A.GetDistance(this.B)) <= this.B.GetDistance(this.C));
            if (isInvalidTriangle)
            {
                Console.WriteLine("Incorrect Triangle");
                Console.WriteLine();
            }
        }

        public override double GetPerimeter()
        {
            return this.A.GetDistance(this.B) + this.B.GetDistance(this.C) + this.A.GetDistance(this.C);
        }

        public override void Draw()
        {
            Console.WriteLine("Triangle");
            Console.WriteLine($"Point 1 coordinates: ({A.X}, {A.Y})");
            Console.WriteLine($"Point 2 coordinates: ({B.X}, {B.Y})");
            Console.WriteLine($"Point 3 coordinates: ({C.X}, {C.Y})");
            Console.WriteLine($"First side = {A.GetDistance(B)}");
            Console.WriteLine($"Second side = {B.GetDistance(C)}");
            Console.WriteLine($"Third side = {C.GetDistance(A)}");
            Console.WriteLine($"Perimeter = {GetPerimeter()}");
            Console.WriteLine();
        }
    }
}
