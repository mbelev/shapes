namespace Shapes.Shapes.Polygon
{
    using System;

    public class Square : Polygon
    {
        public double Side { get; set; }

        public Point BottomRightPoint { get; set; }

        public Square(double x, double y, double side)
        {
            this.Side = side;
            this.BottomRightPoint = new Point(x, y);
        }

        public override double GetPerimeter()
        {
            return 4 * this.Side;
        }

        public override void Draw()
        {
            Console.WriteLine("Square");
            Console.WriteLine($"Lower right point coordinates: ({BottomRightPoint.X}, {BottomRightPoint.Y})");
            Console.WriteLine($"Side = {Side}");
            Console.WriteLine($"Perimeter = {GetPerimeter()}");
            Console.WriteLine();
        }
    }
}
