namespace Shapes.Shapes.Polygon
{
    using System;

    public class Rectangle : Polygon
    {
        public double Width { get; set; }

        public double Height { get; set; }

        public Point BottomRightPoint { get; set; }

        public Rectangle(double x, double y, double width, double height)
        {
            this.Width = width;
            this.Height = height;
            this.BottomRightPoint = new Point(x, y);
        }

        public override double GetPerimeter()
        {
            return 2 * (this.Width + this.Height);
        }

        public override void Draw()
        {
            Console.WriteLine("Rectangle");
            Console.WriteLine($"Lower right point coordinates: ({BottomRightPoint.X}, {BottomRightPoint.Y})");
            Console.WriteLine($"Width = {Width}");
            Console.WriteLine($"Height = {Height}");
            Console.WriteLine($"Perimeter = {GetPerimeter()}");
            Console.WriteLine();
        }
    }
}
