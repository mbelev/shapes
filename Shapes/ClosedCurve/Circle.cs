namespace Shapes.Shapes.ClosedCurve
{
    using System;

    public class Circle : ClosedCurve
    {
        public double Radius { get; set; }
        public Point Center { get; set; }

        public Circle(double centerX, double centerY, double radius)
        {
            this.Radius = radius;
            this.Center = new Point(centerX, centerY);
        }

        public override double GetPerimeter()
        {
            return 2 * Math.PI * this.Radius;
        }

        public override void Draw()
        {
            Console.WriteLine("Circle");
            Console.WriteLine($"Radius = {Radius}");
            Console.WriteLine($"Center coordinates: ({Center.X}, {Center.Y})");
            Console.WriteLine($"Perimeter = {GetPerimeter()}");
            Console.WriteLine();
        }
    }
}
