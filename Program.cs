﻿namespace Shapes
{
    using System;
    using System.Collections.Generic;

    using Shapes;
    using Shapes.Polygon;
    using Shapes.ClosedCurve;

    class Program
    {
        static void Main(string[] args)
        {
            var shapes = new List<Shape>();
            shapes.Add(new Rectangle(8.1, 9, 2.5, 1.5));
            shapes.Add(new Square(5.2, 12.5, 10));
            shapes.Add(new Triangle(1, 1, 3, 1, 1, 4));
            shapes.Add(new Circle(8, 12.5, 9.0));

            foreach (var shape in shapes)
            {
                Console.WriteLine("Draw object ");
                shape.Draw();
            }
        }
    }
}
